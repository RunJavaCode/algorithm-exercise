# 简介
日常算法练习与学习记录。

使用的编程语言是java。

所有的算法题目都是来自 [leetcode](https://leetcode.com/) 官网算法部分 [leetcode-Algorithms](https://leetcode.com/problemset/algorithms/) 

# 练习的题目列表
|编号|题目|leetcode难度|题解与分析|原题链接|
|:---:|:---:|:---:|:---:|:---:|
|1|求二进制和|简单|[题解与分析](src/main/java/algorithmEasy/addBinary/AddBinary.java)|[点击跳转](https://leetcode.com/problems/add-binary/)|
|2|实现 strStr()|简单|[题解与分析](src/main/java/algorithmEasy/strStr/StrStr.java)|[点击跳转](https://leetcode.com/problems/implement-strstr/)|
|3|实现 Sqrt(x)|简单|[题解与分析](src/main/java/algorithmEasy/sqrt/Sqrt.java)|[点击跳转](https://leetcode.com/problems/sqrtx/)|
|4|爬楼梯|简单|[题解与分析](src/main/java/algorithmEasy/climbStairs/ClimbingStairs.java)|[点击跳转](https://leetcode.com/problems/climbing-stairs/)|
|5|对已排序的链表去重|简单|[题解与分析](src/main/java/algorithmEasy/removeDuplicatesFromSortedList/RemoveDuplicatesFromSortedList.java)|[点击跳转](https://leetcode.com/problems/remove-duplicates-from-sorted-list/)|
|6|合并已排序数组|简单|[题解与分析](src/main/java/algorithmEasy/mergeSortedArray/MergeSortedArray.java)|[点击跳转](https://leetcode.com/problems/merge-sorted-array/)|
|7|中序遍历二叉树|简单|[题解与分析](src/main/java/binaryTree/binaryTreeInOrderTraversal/BinaryTreeInOrderTraversal.java)|[点击跳转](https://leetcode.com/problems/binary-tree-inorder-traversal/)|
|8|相同树判断|简单|[题解与分析](src/main/java/binaryTree/sameTree/SameTree.java)|[点击跳转](https://leetcode.com/problems/same-tree/)|
|9|判断树是否对称|简单|[题解与分析](src/main/java/binaryTree/symmetricTree/SymmetricTree.java)|[点击跳转](https://leetcode.com/problems/symmetric-tree/)|
|10|求树的最大深度|简单|[题解与分析](src/main/java/binaryTree/maximumDepthOfBinaryTree/MaximumDepthOfBinaryTree.java)|[点击跳转](https://leetcode.com/problems/maximum-depth-of-binary-tree/)|
|11|把已排序数组转换成二叉搜索树|简单|[题解与分析](src/main/java/binaryTree/convertSortedArrayToBinarySearchTree/ConvertSortedArrayToBinarySearchTree.java)|[点击跳转](https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/)|
|12|求树的最小深度|简单|[题解与分析](src/main/java/binaryTree/minimumDepthOfBinaryTree/MinimumDepthOfBinaryTree.java)|[点击跳转](https://leetcode.com/problems/minimum-depth-of-binary-tree/)|
|13|路径之和|简单|[题解与分析](src/main/java/algorithmEasy/pathSum/PathSum.java)|[点击跳转](https://leetcode.com/problems/path-sum/)|
|14|帕斯卡三角形|简单|[题解与分析](src/main/java/algorithmEasy/pascalsTriangle/PascalsTriangle.java)|[点击跳转](https://leetcode.com/problems/pascals-triangle/)|
|15|帕斯卡三角形II|简单|[题解与分析](src/main/java/algorithmEasy/pascalsTriangleII/PascalsTriangleII.java)|[点击跳转](https://leetcode.com/problems/pascals-triangle-ii/)|
|16|最佳买卖股票时机|简单|[题解与分析](src/main/java/algorithmEasy/bestTimeToBuyAndSellStock/BestTimeToBuyAndSellStock.java)|[点击跳转](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/)|
|17|找出单个数字|简单|[题解与分析](src/main/java/algorithmEasy/singleNumber/SingleNumber.java)|[点击跳转](https://leetcode.com/problems/single-number/)|
|18|二叉树前序遍历|简单|[题解与分析](src/main/java/binaryTree/binaryTreePreorderTraversal/BinaryTreePreorderTraversal.java)|[点击跳转](https://leetcode.com/problems/binary-tree-preorder-traversal/)|
|19|二叉树后序遍历|简单|[题解与分析](src/main/java/binaryTree/binaryTreePostorderTraversal/BinaryTreePostorderTraversal.java)|[点击跳转](https://leetcode.com/problems/binary-tree-postorder-traversal/)|
|20|Min Stack|简单|[题解与分析](src/main/java/algorithmEasy/MinStack/MinStack.java)|[点击跳转](https://leetcode.com/problems/min-stack/)|
|21|求两个单向链表的交点|简单|[题解与分析](src/main/java/algorithmEasy/intersectionOfTwoLinkedLists/IntersectionOfTwoLinkedLists.java)|[点击跳转](https://leetcode.com/problems/intersection-of-two-linked-lists/)|
|22|两数之和II|简单|[题解与分析](src/main/java/algorithmEasy/twoSumII/TwoSumII.java)|[点击跳转](https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/)|
|23|找出数组中主要元素|简单|[题解与分析](src/main/java/algorithmEasy/majorityElement/MajorityElement.java)|[点击跳转](https://leetcode.com/problems/majority-element/)|
|24|excel表格列数值|简单|[题解与分析](src/main/java/algorithmEasy/excelSheetColumnNumber/ExcelSheetColumnNumber.java)|[点击跳转](https://leetcode.com/problems/excel-sheet-column-number/)|
|25|快乐的数字|简单|[题解与分析](src/main/java/algorithmEasy/happyNumber/HappyNumber.java)|[点击跳转](https://leetcode.com/problems/happy-number/)|
|26|1bit位的个数|简单|[题解与分析](src/main/java/algorithmEasy/numberOf1Bits/NumberOf1Bits.java)|[点击跳转](https://leetcode.com/problems/number-of-1-bits/)|
|27|移除Linked List中重复元素|简单|[题解与分析](src/main/java/algorithmEasy/removeLinkedListElements/RemoveLinkedListElements.java)|[点击跳转](https://leetcode.com/problems/remove-linked-list-elements/)|
|28|反转单链表|简单|[题解与分析](src/main/java/algorithmEasy/reverseLinkedList/ReverseLinkedList.java)|[点击跳转](https://leetcode.com/problems/reverse-linked-list/)|
|29|判断数组是否包含重复|简单|[题解与分析](src/main/java/algorithmEasy/containsDuplicate/ContainsDuplicate.java)|[点击跳转](https://leetcode.com/problems/contains-duplicate/)|
|30|判断数组是否包含重复II|简单|[题解与分析](src/main/java/algorithmEasy/containsDuplicateII/ContainsDuplicateII.java)|[点击跳转](https://leetcode.com/problems/contains-duplicate-ii/)|
|31|缺失的数字|简单|[题解与分析](src/main/java/algorithmEasy/missingNumber/MissingNumber.java)|[点击跳转](https://leetcode.com/problems/missing-number/)|
|32|移动所有0值|简单|[题解与分析](src/main/java/algorithmEasy/moveZeroes/MoveZeroes.java)|[点击跳转](https://leetcode.com/problems/move-zeroes/)|
|33|范围和查询|简单|[题解与分析](src/main/java/algorithmEasy/rangeSumQuery/RangeSumQuery.java)|[点击跳转](https://leetcode.com/problems/range-sum-query-immutable/)|
|34|两个数组的交集|简单|[题解与分析](src/main/java/array/intersectionOfTwoArrays/IntersectionOfTwoArrays.java)|[点击跳转](https://leetcode.com/problems/intersection-of-two-arrays/)|
|35|找到数组中没有出现的数字|简单|[题解与分析](src/main/java/array/findAllNumbersDisappearedInAnArray/FindAllNumbersDisappearedInAnArray.java)|[点击跳转](https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/)|
|36|找到数组中出现两次的数字|Medium|[题解与分析](src/main/java/array/findAllDuplicatesInAnArray/FindAllDuplicatesInAnArray.java)|[点击跳转](https://leetcode.com/problems/find-all-duplicates-in-an-array/)|
|37|下一个较大的数字|简单|[题解与分析](src/main/java/array/nextGreaterElementI/NextGreaterElementI.java)|[点击跳转](https://leetcode.com/problems/next-greater-element-i/)|
|38|键盘行|简单|[题解与分析](src/main/java/array/keyboardRow/KeyboardRow.java)|[点击跳转](https://leetcode.com/problems/keyboard-row/)|
