package algorithmEasy.strStr;

/**
 * 题目描述：
 * 实现一个功能类似C++的strStr()函数
 * @see <a href="http://www.cplusplus.com/reference/cstring/strstr/">C++ strStr()</a>
 *
 * 示例：
 * 输入: haystack = "hello", needle = "ll"
 * 输出: 2
 *
 * 输入: haystack = "aaaaa", needle = "bba"
 * 输出: -1
 *
 * 输入: haystack = "", needle = ""
 * 输出: 0
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/implement-strstr/">Implement strStr</a>
 *
 */
public class StrStr {

    /**
     * 实现方法一
     *
     */
    public int strStrImpl1(String haystack, String needle) {
        int l1 = haystack.length(), l2 = needle.length();
        if (l2 == 0) {
            return 0;
        }
        for (int i = 0; i <= l1 - l2; i++) {
            if (haystack.substring(i, i + l2).equals(needle)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 实现方法二
     *
     */
    public int strStrImpl2(String haystack, String needle) {
        for (int i = 0; ; i++) {
            for (int j = 0; ; j++) {
                if (j == needle.length())
                    return i;
                if (i + j == haystack.length())
                    return -1;
                if (needle.charAt(j) != haystack.charAt(i + j))
                    break;
            }
        }
    }
}
