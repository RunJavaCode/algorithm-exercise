package algorithmEasy.excelSheetColumnNumber;

/**
 * 题目描述：
 * 给定一个在excel表格中代表列标题头的字符串 columnTitle, 返回它所对应的 列值(column number)。
 * 比如：
 * A -> 1
 * B -> 2
 * C -> 3
 * ...
 * Z -> 26
 * AA -> 27
 * AB -> 28
 *
 * 示例：
 * 输入：columnTitle = "A"
 * 输出：1
 *
 * 输入：columnTitle = "AB"
 * 输出：28
 *
 * 输入：columnTitle = "FXSHRXW"
 * 输出：2147483647
 *
 * 限制条件：
 * 1 <= columnTitle.length <= 7
 * columnTitle 只有大写的英文字母
 * columnTitle 范围在 ["A", "FXSHRXW"].
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/excel-sheet-column-number/">Excel Sheet Column Number</a>
 */
public class ExcelSheetColumnNumber {

    /**
     * 发现规律才能做到这题目。
     * 参考：https://leetcode.com/problems/excel-sheet-column-number/discuss/52154/Concise-java-solution-with-explanation.
     * "B" = 2
     * "BC" = (2)26 + 3
     * "BCM" = (2(26) + 3)26 + 13
     */
    public int titleToNumber0(String columnTitle) {
        int sum = 0;
        for (char c : columnTitle.toCharArray()) {
           sum *= 26;
           // 利用 ascii 字符相减  A -> 65  ... Z -> 90
           sum += c - 'A' + 1;
        }
        return sum;
    }

    /**
     * 写法二
     */
    public int titleToNumber1(String columnTitle) {
        int result = 0;
        for (int i = 0; i < columnTitle.length(); i++) {
            result = result * 26 + (columnTitle.charAt(i) - 'A' + 1);
        }
        return result;
    }

}
