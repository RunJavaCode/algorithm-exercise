package algorithmEasy.addBinary;

/**
 * 题目描述:
 * 给定二进制字符串a和b, 返回它们和的二进制数。
 *
 * 示例:
 * 输入: a = "11", b = "1"
 * 输出: "100"
 *
 * 输入: a = "1010", b = "1011"
 * 输出: "10101"
 *
 * 原题链接:
 * @see <a href="https://leetcode.com/problems/add-binary/">add-binary</a>
 */
public class AddBinary {

    /**
     * 实现方法一
     *
     */
    public String addBinaryImpl1(String a, String b) {
        char[] aChar = a.toCharArray();
        char[] bChar = b.toCharArray();
        int aSum = 0, bSum = 0;
        for (int i = aChar.length - 1; i >= 0; i--) {
            aSum += Character.getNumericValue(aChar[i]) * Math.pow(2, i);
        }
        for (int i = bChar.length - 1; i >= 0; i--) {
            bSum += Character.getNumericValue(bChar[i]) * Math.pow(2, i);
        }
        int i = aSum + bSum;
        return Integer.toBinaryString(i);
    }

    /**
     * 实现方法二
     *
     */
    public String addBinaryImpl2(String a, String b) {
        StringBuilder sb = new StringBuilder();
        int i = a.length() - 1, j = b.length() - 1, carry = 0;
        while (i >= 0 || j >= 0) {
            int sum = carry;
            if (j >= 0) {
                sum += b.charAt(j--) - '0';
            }
            if (i >= 0) {
                sum += a.charAt(i--) - '0';
            }
            sb.append(sum % 2);
            carry = sum / 2;
        }
        if (carry == 0) {
            return sb.reverse().toString();
        }
        sb.append(carry);
        return sb.reverse().toString();
    }
}
