package algorithmEasy.mergeSortedArray;

/**
 * 题目描述：
 * 给定两个以非递减顺序的integer数组nums1, nums2, 以及代表nums1、nums2数组内部元素数量的值m、n。
 * 合并两个数组到非递减顺序的单个数组里。
 * 注意：最终的结果并不是以函数返回值返回的，而是保存在nums1里面。
 *
 * 示例一
 * 输入：nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
 * 输出：[1,2,2,3,5,6]
 *
 * 示例二
 * 输入：nums1 = [1], m = 1, nums2 = [], n = 0
 * 输出：[1]
 *
 * 示例二
 * 输入：nums1 = [0], m = 0, nums2 = [1], n = 1
 * 输出：[1]
 * 注意：因为m = 0，意味着nums1里面没有元素。里面的0只是为了确保合并的结果可以放入nums1里面。
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/merge-sorted-array/">Merge Sorted Array</a>
 */
public class MergeSortedArray {

    /**
     * 实现一
     */
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        // 命名三个变量，分别是三个数组的最大索引值大小
        int i1 = m - 1, i2 = n - 1, total = m + n - 1;
        // 持续比较两个数组里面的值的大小，直到其中一个数组内的元素比完结束。
        while (i1 >= 0 && i2 >= 0) {
            // nums1[i--]的用法还是第一次见。它会打印当前i所在的值，再自减。
            nums1[total--] = (nums1[i1] > nums2[i2]) ? nums1[i1--] : nums2[i2--];
        }
        // 只需检验nums2数组即可。
        // 因为题目要求最终结果是在nums1里面。
        while (i2 >= 0) {
            nums1[total--] = nums2[i2--];
        }
    }

    /**
     * 实现二
     * 这写法很简洁
     */
    public static void merge2(int[] nums1, int m, int[] nums2, int n) {
        for (int i = m - 1, j = n - 1, k = m + n - 1; k >= 0 && j >= 0; k--)
            nums1[k] = (i < 0 || nums1[i] < nums2[j] ? nums2[j--] : nums1[i--]);
    }

}
