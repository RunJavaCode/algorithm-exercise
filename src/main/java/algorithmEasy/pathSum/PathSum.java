package algorithmEasy.pathSum;

import java.util.Stack;

/**
 * 题目描述：
 * 给定一个二叉树root以及一个整数targetSum,
 * 如果树中有一条根到叶的路径使得沿着该路径所有值之和等于targetSum,返回true。
 *
 * 示例：
 * 输入：root = [5,4,8,11,null,13,4,7,2,null,null,null,1]
 *      targetSum = 22
 * 输出：true
 *
 * 输入：root = [1,2,3]
 *      targetSum = 5
 * 输出：false
 *
 * 输入：root = [1,2]
 *      targetSum = 0
 * 输出：false
 *
 * 约束条件：
 * - 二叉树的节点的数量范围在[0, 5000]
 * - -1000 <= Node.val <= 1000
 * - 1000 <= targetSum <= 1000
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/path-sum/">Path Sum</a>
 */
public class PathSum {

    /**
     * 迭代的方式一
     */
    public boolean hasPathSum(TreeNode root, int targetSum) {
        if (root == null)
            return false;
        // 到达叶子节点，其值等于最后目标值, 意味着存在这样一条根到叶的路径。
        if (root.left == null && root.right == null && root.val == targetSum)
            return true;
        // 剩下的值总和 -> 目标值减去当前节点的值。
        int restSum = targetSum - root.val;
        return hasPathSum(root.left, restSum) || hasPathSum(root.right, restSum);
    }

    /**
     * 迭代的方式二
     */
    public boolean hasPathSum0(TreeNode root, int targetSum) {
       return traverse(root, 0, targetSum);
    }
    private boolean traverse(TreeNode root, int temp, int targetSum) {
        if (root == null) return false;
        // 到达叶子节点
        if (root.left == null && root.right == null)
            return targetSum == root.val + temp;
        // 中间值累加
        temp += root.val;
        // 对左右子树分别进行遍历
        boolean left = traverse(root.left, temp, targetSum);
        boolean right = traverse(root.right, temp, targetSum);
        return left || right;
    }

    /**
     * 迭代的方式一
     */
    public boolean hasPathSum1(TreeNode root, int targetSum) {
        // 用来存放已经访问过的节点
        Stack<TreeNode> visitedNodes = new Stack<>();
        TreeNode prev = null;

        while (root != null || !visitedNodes.isEmpty()) {
            while (root != null) {
                visitedNodes.push(root);
                targetSum -= root.val;
                prev = root;
                root = root.left;
            }
            root = visitedNodes.peek();
            // 最终决定条件：到叶子节点时的路径和 与 目标和 是否相等
            if (root.left == null && root.right == null && targetSum == 0)
                return true;
            // root.right != prev 这样就不必访问已访问过的右节点。
            if (root.right != null && root.right != prev)
                root = root.right;
            else {
                targetSum += root.val;
                prev = visitedNodes.pop();
                // 这样就不必访问已经访问过的左节点。
                root = null;
            }
        }
        return false;
    }

    /**
     * 迭代的方式二
     */
    public boolean hasPathSum2(TreeNode root, int targetSum) {
        if (root == null)
            return false;
        // 存储'所有节点'的stack
        Stack<TreeNode> stack = new Stack<>();
        // 存储'路径和'的stack
        Stack<Integer> sum = new Stack<>();
        stack.push(root);
        sum.push(root.val);
        // 对stack进行遍历
        while (!stack.isEmpty()) {
            TreeNode current = stack.pop();
            // 取出此时的路径和
            int path = sum.pop();
            if (current.left != null) {
                // 把左节点push进保存节点的stack中
                stack.push(current.left);
                // 把"路径和"push进sum所在的stack中
                sum.push(path + current.left.val);
            }
            if (current.right != null) {
                stack.push(current.right);
                sum.push(path + current.right.val);
            }
            // 最终决定条件：到叶子节点时的路径和 与 目标和 是否相等
            if (current.left == null && current.right == null && path == targetSum)
                return true;
        }
        return false;
    }

    /*
     * 复习一下：前、中、后序递归。
     * 前序：root -> left -> right
     * 中序：left -> root -> right
     * 后序：left -> right -> root
     */
    public void preOrder(TreeNode root) {
        // base condition
        if (root == null) return;
        System.out.println(root.val);
        preOrder(root.left);
        preOrder(root.right);
    }
    public void inOrder(TreeNode root) {
        if (root == null) return;
        inOrder(root.left);
        System.out.println(root.val);
        inOrder(root.right);
    }
    public void postOrder(TreeNode root) {
        if (root == null) return;
        postOrder(root.left);
        postOrder(root.right);
        System.out.println(root.val);
    }

}
