package algorithmEasy.pascalsTriangle;

import java.util.ArrayList;
import java.util.List;

/**
 * 题目描述：
 * 给一个整数numRows， 返回 “帕斯卡三角形”。
 * 在帕斯卡三角形中, 每个数字都是其正上方的两个数字之和。
 * 可以Google查看具体的图形示意。
 *
 * 示例：
 * 输入：numRows = 5
 * 输出：[[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
 *
 * 输入：numsRows = 1
 * 输出：[[1]]
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/pascals-triangle/"> Pascal's Triangle </a>
 */
public class PascalsTriangle {

    /**
     * 解法三
     */
    public List<List<Integer>> generate2(int numRows) {
        // 保存最终结果
        List<List<Integer>> result = new ArrayList<>();
        // 循环每一层
        for (int i = 0; i < numRows; i++) {
            List<Integer> row = new ArrayList<>();
            // 每一层的赋值。 下一层是上一层多 1
            for (int j = 0; j < i + 1; j++) {
                // 如果是第一个或者最后一个赋值为 1
                if (j == 0 || j == i)
                    row.add(1);
                else {
                    // 取前一行的第j-1个 、第j个相加。这里很巧妙
                    int a = result.get(i - 1).get(j - 1);
                    int b = result.get(i - 1).get(j);
                    row.add(a + b);
                }
            }
            result.add(row);
        }
        return result;
    }

    /**
     * 解法二
     */
    public List<List<Integer>> generate1(int numRows) {
        // 保存最终结果
        List<List<Integer>> allRows = new ArrayList<>();
        // 每一行
        List<Integer> row = new ArrayList<>();
        // 遍历numRows层
        for (int i = 0; i < numRows; i++) {
            // 使用上一行的row的大小来判断是否需要继续遍历添加行
            for (int j = row.size() - 1; j >= 1; j--)
                row.set(j, row.get(j) + row.get(j - 1));
            row.add(1);
            allRows.add(new ArrayList<>(row));
//            另一种for循环做法
//            row.add(0, 1);
//            for (int j = 1; j < row.size() - 1; j++)
//                row.set(j, row.get(j) + row.get(j + 1));
//            allRows.add(new ArrayList<>(row));
        }
        return allRows;
    }


    /**
     * 解法一
     */
    public List<List<Integer>> generate0(int numRows) {
        // 最后的结果
        List<List<Integer>> result = new ArrayList<>();
        // 放第一个值
        List<Integer> one = new ArrayList<>();
        one.add(1);
        result.add(one);
        // 临时数组，保存将要用来生成后一个数组的数组
        List<Integer> tempList = new ArrayList<>();
        tempList.add(0, 1);
        tempList.add(1, 1);
        // 表示层级数
        int start = 1;
        while (start < numRows) {
            List<Integer> ll = new ArrayList<>(numRows);
            // 添加首个元素
            ll.add(0, 1);
            for (int i = 1; i < start; i++) {
                ll.add(i, tempList.get(i - 1) + tempList.get(i));
            }
            // 添加最后一个元素
            ll.add(start, 1);
            // 层级递增
            start++;
            // 把当前生成的数组赋给临时数组, 用于生成下一行
            tempList = ll;
            // 把当前生成的数组添加到结果数组中
            result.add(ll);
        }
        return result;
    }

}