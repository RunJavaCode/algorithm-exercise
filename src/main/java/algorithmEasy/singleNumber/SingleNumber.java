package algorithmEasy.singleNumber;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * 题目描述：
 * 给定一个非空的整数数组nums，每个元素都显示两次，除了一个元素。找出那一个元素。
 * 您必须实现具有线性运行时复杂性的解决方案，并且只使用恒定的额外空间。
 *
 * 示例：
 * 输入：nums = [2,2,1]
 * 输出：1
 *
 * 输入：nums = [4,1,2,1,2]
 * 输出：4
 *
 * 输入：nums = [1]
 * 输出：1
 *
 * 原题链接：
 *
 * @see <a href="https://leetcode.com/problems/single-number/">Single Number</a>
 */
public class SingleNumber {

    /**
     * 自己实现使用的hashMap
     */
    public int singleNumber(int[] nums) {
        if (nums.length == 1) return nums[0];
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            if (map.containsKey(num)) {
                map.put(num, map.get(num) + 1);
            } else {
                map.put(num, 1);
            }
        }
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 1)
                return entry.getKey();
        }
        return nums[0];
    }

    /**
     * 网友的解答真的简洁高效, 使用XOR来解这道题。
     * 学习一下XOR是什么：
     * 按位异或： 1. 0^N = N
     *          2. N^N = 0
     * 所以，如果N是single Number，那么
     *  N1^N1^N2^N2……^Nx^Nx^N
     *  = (N1^N1)^(N2^N2)……^(Nx^Nx)^N
     *  = 0^0^……^0^N
     *  = N
     */
    public int singleNumber1(int[] nums) {
        int ans = 0;
        int len = nums.length;

        for (int i = 0; i != len; i++)
            ans ^= nums[i];

        return ans;
    }

    /**
     * 使用HashSet
     */
    public int singleNumber2(int[] nums) {
        final HashSet<Integer> set = new HashSet<>();
        for (int num : nums) {
            // 如果被删除了，返回true。所以这是重复的值。
            boolean isDuplicate = set.remove(num);
            // 把不重复的值添加进set
            if (!isDuplicate)
                set.add(num);
        }
        // 把set内唯一的值返回
        return set.iterator().next();
    }

}
