package algorithmEasy.reverseLinkedList;

import java.util.Stack;

/**
 * 题目描述：
 * 给定一个单向链表head, 反转链表并返回。
 *
 * 示例：
 * 输入：head = [1,2,3,4,5]
 * 输出：[5,4,3,2,1]
 *
 * 输入：head = [1,2]
 * 输出：[2,1]
 *
 * 输入：head = []
 * 输出：[]
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/reverse-linked-list/"> Reverse Linked List </a>
 */
public class ReverseLinkedList {

    /**
     * 迭代的方法
     */
    public ListNode reverseList0(ListNode head) {
        ListNode result = null;
        while (head != null) {
            // head.next代表当前head节点后面的所有节点
            ListNode next = head.next;
            // 做交换。
            head.next = result;
            result = head;
            // 把剩下的节点继续赋值给head的，进行新一轮循环
            head = next;
        }
        return result;
    }

    /**
     * 递归的方法
     */
    public ListNode reverseList1(ListNode head) {
        return reverseListInt(head, null);
    }

    private ListNode reverseListInt(ListNode head, ListNode newHead) {
        if (head == null) return newHead;
        ListNode next = head.next;
        head.next = newHead;
        return reverseListInt(next, head);
    }

    /**
     * 递归方法二
     */
    public ListNode reverseList2(ListNode head) {
        // 基本条件
        if (head == null || head.next == null) return head;

        ListNode newHead = reverseList2(head.next);

        head.next.next = head;
        head.next = null;

        return newHead;
    }

}
