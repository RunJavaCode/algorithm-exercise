package algorithmEasy.numberOf1Bits;

/**
 * 题目描述：
 * 编写一个函数，该函数接受一个无符号的整数，并且返回该整数‘1’bits的个数(也被称为Hamming weight)。
 * @see <a href="https://en.wikipedia.org/wiki/Hamming_weight"> Hamming weight </a>
 *
 * 示例：
 * 输入：n = 00000000000000000000000000001011
 * 输出：3
 *
 * 输入：n = 00000000000000000000000010000000
 * 输出：1
 *
 * 输入：n = 11111111111111111111111111111101
 * 输出：31
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/number-of-1-bits/"> Number Of 1 Bits </a>
 */
public class NumberOf1Bits {

    /**
     * 关于bit运算的一些前置知识：
     * & : 按位与运算符。只有当两个位都为1时，结果才为1。
     * | : 按位或运算符。只有两个位都为0时，结果才为0。
     * ^ : 按位异或运算符。两个位相同为0，相异为1。
     * ~ : 按位取反。0变成1，1变成0。
     * << : 左移。各二进制位全部左移若干位，高位丢弃，低位补0。
     * >> : 右移。各二进制位全部右移若干位，对无符号数，高位补0,有符号数，分两种：补符号位（算术右移），补0（逻辑右移）
     * >>>：无符号右移，Java中特有的一个操作符
     *
     * 以下解答来自：https://leetcode.com/problems/number-of-1-bits/discuss/55099/Simple-Java-Solution-Bit-Shifting
     */
    public int hammingWeight(int n) {
        int result = 0;

        // while循环，直到n所有的bit位都是0。
        while (n != 0) {
            // 将n与1进行按与运算。如果结果是1,代表输入整数n的最后一bit位是1。
            result += (n & 1);
            // 对整数n 进行无符号右移。
            n >>>= 1;
        }

        return result;
    }

    /**
     * 原来int的包装类Integer有一个方法实现了求one-bits的个数的方法。
     * 源自：https://leetcode.com/problems/number-of-1-bits/discuss/55352/Super-simple-Java-solution
     */
    public int hammingWeight1(int n) {
        return Integer.bitCount(n);
    }
}
