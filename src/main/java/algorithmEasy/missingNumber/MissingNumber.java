package algorithmEasy.missingNumber;

import java.util.Arrays;

/**
 * 题目描述：
 * 给定一个包含n个范围值在[0,n]数组nums，返回该范围内唯一一个缺失值。
 *
 * 示例：
 * 输入：nums = [3,0,1]
 * 输出：2
 *
 * 输入：nums = [0,1]
 * 输出：2
 *
 * 输入：nums = [9,6,4,2,3,5,7,0,1]
 * 输出：8
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/missing-number/"> Missing number </a>
 */
public class MissingNumber {

    /**
     * 方法一：先对数组nums进行排序，然后再遍历做判断，当索引值与num[i]不相等时，说明缺失该值;
     * 当循环结束还是没有缺失值时，说明缺失 n + 1 的值。
     */
    public int missingNumber0(int[] nums) {
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i++) {
            if (i != nums[i])
                return i;
        }
        return nums[nums.length - 1] + 1;
    }

    /**
     * 方法二：二分法
     * 对排序后的数组使用二分法
     */
    public int missingNumber1(int[] nums) {
        // 排序
        Arrays.sort(nums);
        // 二分法过程
        int left = 0, right = nums.length, mid = (left + right) / 2;
        while (left < right) {
            mid = (left + right) / 2;
            if (nums[mid] > mid)
                right = mid;
            else
                left = mid + 1;
        }
        return left;
    }


    /**
     * 方法三：可以将 [0, n] 中的所有数字加在一起，然后减去数组中 n-1 个数字的总和。
     */
    public int missingNumber2(int[] nums) {
        int sum = nums.length;
        for (int i = 0; i < nums.length; i++) {
            sum += i - nums[i];
        }
        return sum;
    }
}
