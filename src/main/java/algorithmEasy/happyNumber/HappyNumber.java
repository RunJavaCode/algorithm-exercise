package algorithmEasy.happyNumber;

import java.util.HashSet;
import java.util.Set;

/**
 * 题目描述：
 * 编写一个算法判断一个数字n是否是开心的。
 * 一个开心的数字由如下过程定义：
 *      - 从任何正整数开始,用数字的平方之和来代替这个数字。
 *      - 重复这个过程直到数字等于一（停在这里），
 *        或者它在不包括1的循环中无休止地循环。
 *      - 这个过程以1结束的数字是幸福的。
 * 如果数字n是开心的则返回true，否则返回false.
 *
 * 示例：
 * 输入：n = 19
 * 输出：true
 * 解释：
 * 1^2 + 9^2 = 82
 * 8^2 + 2^2 = 68
 * 6^2 + 8^2 = 100
 * 1^2 + 0^2 + 02 = 1
 *
 * 示例二：
 * 输入：2
 * 输出：false
 *
 * 约束条件：
 * 1 <= n <= 2^31 - 1
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/happy-number/">happy number</a>
 */
public class HappyNumber {

    /**
     * 方法一
     */
    public boolean isHappy0(int n) {
        Set<Integer> inLoop = new HashSet<>();
        // squareSum：平方和， remain：剩余数
        int squareSum, remain;
        // 把n添加入Set集合中
        while (inLoop.add(n)) {
            // 平方和初始值置为0
            squareSum = 0;
            while (n > 0) {
                // 取模得到最后一位数
                remain = n % 10;
                // 求平方和
                squareSum += remain * remain;
                // 求得余数
                n /= 10;
            }
            // 如果平方和等于1,返回true;否则，把平方和赋值给n
            if (squareSum == 1)
                return true;
            else
                n = squareSum;
        }
        return false;
    }

    /**
     * 方法二
     * 网友们的数学太厉害了！又认识到了一个新的算法：Floyd’s Cycle Detection Algorithm
     */
    public boolean isHappy1(int n) {
        int slow, fast;
        slow = fast = n;
        do {
            slow = digitSquareSum(slow);
            fast = digitSquareSum(fast);
            fast = digitSquareSum(fast);
        } while (slow != fast);
        return slow == 1;
    }

    private int digitSquareSum(int n) {
        int sum = 0, tmp;
        while (n > 0) {
            tmp = n % 10;
            sum += tmp * tmp;
            n /= 10;
        }
        return sum;
    }

    /**
     * 使用递归的方式
     */
    public boolean isHappy(int n) {
        if (n == 1 || n == 7) return true;
        else if (n < 10) return false;
        int m = 0;
        while (n != 0) {
            int tail = n % 10;
            m += tail * tail;
            n = n/10;
        }
        return isHappy(m);
    }
}
