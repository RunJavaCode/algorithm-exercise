package algorithmEasy.pascalsTriangleII;

import java.util.ArrayList;
import java.util.List;

/**
 * 题目描述：
 * 给定一个整数rowIndex, 返回"帕斯卡三角形（Pascal's triangle）"第rowIndex行
 * 帕斯卡三角形（Pascal's triangle）:在帕斯卡三角形中，每个数字是直接上方的两个数字的总和
 *
 * 示例：
 * 输入：rowIndex = 3
 * 输出：[1,3,3,1]
 *
 * 输入：rowIndex = 0
 * 输出：[1]
 *
 * 输入：rowIndex = 1
 * 输出：[1,1]
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/pascals-triangle-ii/"> Pascal's Triangle II </a>
 */
public class PascalsTriangleII {

    /**
     * 方法一
     */
    public List<Integer> getRow0(int rowIndex) {
        final ArrayList<Integer> result = new ArrayList<>();
        // 第一层
        result.add(1);
        // rowIndex是从0开始的
        for (int i = 1; i <= rowIndex; i++) {
            // 内层循环
            for (int j = i - 1; j >= i; j--) {
                int temp = result.get(j - 1) + result.get(j);
                result.set(j, temp);
            }
            result.add(1);
        }
        return result;
    }

    /**
     * 方法二
     */
    public List<Integer> getRow1(int rowIndex) {
        // 把pascal三角形看成一个二维数组
        int[][] pascal = new int[rowIndex + 1][rowIndex + 1];
        // 首层
        pascal[0][0] = 1;
        for (int i = 1; i <= rowIndex; i++) {
            for (int j = 0; j <= rowIndex; j++) {
                // 最后一个值
                if (i == j)
                    pascal[i][j] = 1;
                // 第一个值
                else if (j == 0)
                    pascal[i][j] = 1;
                else
                    pascal[i][j] = pascal[i - 1][j - 1] + pascal[i - 1][j];
            }
        }
        final List<Integer> result = new ArrayList<>();
        for (int i = 0; i <= rowIndex; i++)
            result.add(pascal[rowIndex][i]);
        return result;
    }
}
