package algorithmEasy.climbStairs;

/**
 * 题目描述：
 * 爬楼梯，n步可以到达顶部。
 * 假设每次你只能爬一步或两步。
 * 请问你爬上楼梯顶部有多少种不重复的方式？
 *
 * 限制条件： 1 <= n <= 45
 *
 * 示例一：
 * 输入：n=2
 * 输出：2
 * 解释：有两种方式爬到顶部。
 * 1. 1步+1步
 * 2. 2步
 *
 * 示例二：
 * 输入：n=3
 * 输出：3
 * 解释：有三种方式爬到顶部。
 * 1. 1步+1步+1步
 * 2. 1步+2步
 * 2. 2步+1步
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/climbing-stairs/">climbing-stairs</a>
 *
 * 这道题leetcode归类为简单，估计让很多人灰心...
 * 我对这道题实在没有思路，先学习其他人的思路。
 * 把一些实现放在这里作为记录。date: 2021.8.8
 */
public class ClimbingStairs {

    /**
     * 实现方法一： 动态规划法
     * 虽然用图表示出来后很直观的，但是让我自己想，现在还真不能太理解。
     */
    public static int climbStairs0(int n) {
        if (n == 1)
            return 1;
        int[] dp = new int[n + 1];
        dp[1] = 1;
        dp[2] = 2;
        for (int i = 3; i <= n; i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }
        return dp[n];
    }


    /**
     * 实现二：Fibonacci 数字
     * 我对 Fibonacci 有一点熟悉感，但是具体它是什么还得查资料。
     */
    public static int climbStairs1(int n) {
        if (n == 1)
            return 1;
        int first = 1;
        int second = 2;
        for (int i = 3; i <= n; i++) {
            int third = first + second;
            first = second;
            second = third;
        }
        return second;
    }


    /**
     * 实现三：暴力求解
     * 这种方式最简单明了（但是我连这个也没想出并实现出来）。
     * 内部采用了递归方法（递归也需要再好好理解理解）。
     */
    public static int climbStairs2(int n) {
        return climb(0, n);
    }
    public static int climb(int i, int n) {
        if (i > n)
            return 0;
        if (i == n)
            return 1;
        return climb(i + 1, n) + climb(i + 1, n);
    }

}
