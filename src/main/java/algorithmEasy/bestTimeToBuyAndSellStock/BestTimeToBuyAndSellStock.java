package algorithmEasy.bestTimeToBuyAndSellStock;

/**
 * 题目描述：
 * 给定一个数组prices，其中prices[i]是某只股票在第i天的价格。
 * 你想要通过在某一天购买一只股票并且选择在未来的一天卖出那只股票来最大化收益。
 * 返回你可以从交易中获得的最大收益。如果不能获得任何收益，返回0。
 *
 * 示例：
 * 输入: prices = [7,1,5,3,6,4]
 * 输出: 5
 * 解释： 在第二天买入（价格为1），在第五天卖出（价格为6），获得收益6-1=5。
 *
 * 输入: prices = [7,6,4,3,1]
 * 输出: 0
 * 解释：没有交易可以做出。获益为0。
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/best-time-to-buy-and-sell-stock/"> Best Time to Buy and Sell Stock</a>
 */
public class BestTimeToBuyAndSellStock {

    public int maxProfit0(int[] prices) {
        int maxProfit = 0;
        for (int i = 0; i < prices.length - 1; i++) {
            for (int j = i + 1; j < prices.length; j++) {
                int profit = prices[j] - prices[i];
                if (profit > maxProfit)
                    maxProfit = profit;
            }
        }
        return maxProfit;
    }

    public int maxProfit1(int[] prices) {
        int minPrice = Integer.MAX_VALUE;
        int maxProfit = 0;
        for (int price : prices) {
            if (price < minPrice)
                minPrice = price;
            else if (price - minPrice > maxProfit)
                maxProfit = price - minPrice;
        }
        return maxProfit;
    }

    public int maxProfit2(int[] prices) {
        int maxCur = 0, maxSorFar = 0;
        for (int i = 1; i < prices.length; i++) {
            maxCur = Math.max(0, maxCur + (prices[i] - prices[i - 1]));
            maxSorFar = Math.max(maxCur, maxSorFar);
        }
        return maxSorFar;
    }

    public int maxProfit3(int[] prices) {
        int minPrice = prices[0];
        int maxProfit = 0;
        for (int price : prices) {
            minPrice = Math.min(minPrice, price);
            maxProfit = Math.max(maxProfit, price - minPrice);
        }
        return maxProfit;
    }

}
