package algorithmEasy.moveZeroes;

/**
 * 题目描述：
 * 给定一个整型数组nums，把所有的元素0移动到数组末尾，同时，保持非零元素到相对顺序。
 *
 * 示例：
 * 输入： nums = [0,1,0,3,12]
 * 输出： [1,3,12,0,0]
 *
 * 输入： nums = [0]
 * 输出： [0]
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/move-zeroes/"> move zeroes </a>
 */
public class MoveZeroes {

    /**
     * 方法一
     */
    public void moveZeroes0(int[] nums) {
        if (nums == null || nums.length == 0) return;

        // 将非零值尽可能得向前移动
        int insertPos = 0;
        for (int num : nums) {
            if (num != 0)
                nums[insertPos++] = num;
        }

        // 用0填充剩余的空间
        while (insertPos < nums.length) {
            nums[insertPos++] = 0;
        }
    }

    /**
     * 方法二
     */
    public void moveZeroes1(int[] nums) {
        if (nums == null || nums.length == 0) return;

        int cur = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                // 两个值交换
                int temp = nums[cur];
                nums[cur++] = nums[i];
                nums[i] = temp;
            }
        }
    }

    /**
     * 方法三
     * 这个是我最喜欢的解法
     */
    public void moveZeroes2(int[] nums) {
        // 插入索引值
        int insertPos = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                // 如果当前值所对应的索引值不在待插入索引值位置上
                if (i != insertPos) {
                    // 将当前值换到待插入索引位置上
                    nums[insertPos] = nums[i];
                    // 将当前值置为0
                    nums[i] = 0;
                }
                // 自增插入索引值
                insertPos++;
            }
        }
    }

}
