package algorithmEasy.twoSumII;

/**
 * 题目描述：
 * 给定一个非降序顺序已排序的整数数组,以及一个target值，求出数组中两个元素之和满足目标target值的元素下标（以1开头）。
 *
 * 示例：
 * Input: numbers = [2,7,11,15], target = 9
 * Output: [1,2]
 * Explanation: The sum of 2 and 7 is 9. Therefore index1 = 1, index2 = 2.
 *
 * Input: numbers = [-1,0], target = -1
 * Output: [1,2]
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/"> Two Sum II - Input array is sorted</a>
 */
public class TwoSumII {

    /**
     * 解法一：双层for循环遍历
     * 速度非常慢。
     */
    public int[] twoSum(int[] numbers, int target) {
        // 定义一个包含2个值的数组作为返回结果
        final int[] answer = new int[2];
        int a1 = 1, b1 = 2;
        a:
        for (int i = 0; i < numbers.length; i++) {
            a1 = i + 1;
            // 倒序往前
            for (int j = numbers.length - 1; j > i; j--) {
                b1 = j + 1;
                // 比较和是否等于目标值
                if (numbers[i] + numbers[j] == target) {
                    break a;
                }
            }
        }
        answer[0] = a1;
        answer[1] = b1;
        return answer;
    }

    /**
     * 更简洁的双层for循环
     */
    public int[] twoSum1(int[] numbers, int target) {
        for (int i = 0; i < numbers.length; i++) {
            for (int j = i + 1; j < numbers.length; j++) {
                if (numbers[i] + numbers[j] == target)
                    return new int[]{i + 1, j + 1};
            }
        }
        return new int[]{-1, -1};
    }

    /**
     * 解法二：使用while循环
     * 非常快的解法。
     */
    public int[] twoSum2(int[] numbers, int target) {
        int left = 0, right = numbers.length - 1;
        while (left < right) {
            if (numbers[left] + numbers[right] == target)
                return new int[]{left + 1, right + 1};
                // 利用了题目所说的 numbers 是一个”已经排序好的非递减顺序”的整数数组。
                // 如果两数之和小于目标值，说明左边的值太小了，左指针往右移动; 反之，右指针往左移。很巧妙。
            else if (numbers[left] + numbers[right] < target)
                left++;
            else
                right--;
        }
        return new int[]{-1, -1};
    }

    /**
     * 解法三：使用二分法
     */
    public int[] twoSum3(int[] numbers, int target) {
        for (int i = 0; i < numbers.length; i++) {
            int left = i + 1, right = numbers.length - 1;
            int tmp = target - numbers[i];
            while (left <= right) {
                int mid = left + (right - left) / 2;
                if (numbers[mid] == tmp)
                    return new int[]{i + 1, mid + 1};
                else if (numbers[mid] < tmp)
                    left = mid + 1;
                else
                    right = mid - 1;
            }
        }
        return new int[]{-1, -1};
    }

}
