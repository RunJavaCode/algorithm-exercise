package algorithmEasy.majorityElement;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 题目描述：
 * 给定一个大小为n的数组nums，返回主要的元素.
 * 主要的元素是指：元素出现次数超过 ⌊n / 2⌋ 次。你可以假设主要的元素总是存在数组中。
 *
 * 示例：
 * 输入：nums=[3,2,3]
 * 输出：3
 *
 * 输入：nums=[2,2,1,1,1,2,2]
 * 输出：2
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/majority-element/">Majority Element</a>
 */
public class MajorityElement {

    /**
     * 方法一：我的实现使用HashMap
     */
    public int majorityElement(int[] nums) {
        HashMap<Integer, Integer> map = new HashMap<>();
        map.put(nums[0], 1);
        for (int i = 1; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                // 已经包含该key，value对应的增加1
                map.put(nums[i], map.get(nums[i]) + 1);
            } else {
                map.put(nums[i], 1);
            }
        }
        // 找到value最大值
        int max = Integer.MIN_VALUE;
        for (Map.Entry<Integer, Integer> integerIntegerEntry : map.entrySet()) {
            max = Math.max(max, integerIntegerEntry.getValue());
        }
        // 根据value最大值获取对应的key值，该key值就是出现最多的元素。
        int most = 0;
        for (Integer i : map.keySet()) {
            if (map.get(i).equals(max)) {
                most = i;
            }
        }
        return most;
    }

    /**
     * 方法二: 直接暴力求解法
     */
    public int majorityElement1(int[] nums) {
        // 根据题目提示，主要的元素是指 出现次数大于⌊n / 2⌋次的元素。
        int majorityCount = nums.length / 2;

        // 双层循环遍历，找到出现次数最大的元素
        for (int num : nums) {
            int count = 0;
            for (int elem : nums) {
                if (elem == num) {
                    count += 1;
                }
            }

            // 超过majorityCount,就说明是主要元素。
            if (count > majorityCount) {
                return num;
            }
        }
        // 使用-1表示不存在的结果
        return -1;
    }

    /**
     * 方法三：也是使用HashMap。不过是在取key值时更巧妙。
     */
    public int majorityElement2(int[] nums) {
        Map<Integer, Integer> counts = new HashMap<>();
        for (int num : nums) {
            if (!counts.containsKey(num)) {
                counts.put(num, 1);
            }
            else {
                counts.put(num, counts.get(num)+1);
            }
        }

        Map.Entry<Integer, Integer> majorityEntry = null;
        for (Map.Entry<Integer, Integer> entry : counts.entrySet()) {
            if (majorityEntry == null
                    || entry.getValue() > majorityEntry.getValue()) {
                // 具有更大value值的entry
                majorityEntry = entry;
            }
        }
        return majorityEntry.getKey();
    }

    /**
     * 方法四：排序
     * 因为题目说明了“主要元素”的定义是 出现超过n/2次的元素。
     * 所以，在排序后，找到中间的值，即使“主要元素”
     */
    public int majorityElement3(int[] nums) {
        // 使用Arrays的 sort排序方法，对数组进行排序
        Arrays.sort(nums);
        // 取到中间值，即是最终结果
        return nums[nums.length / 2];
    }
}
