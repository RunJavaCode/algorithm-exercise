package algorithmEasy.containsDuplicateII;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * 题目描述：
 * 给定一个整数数组nums以及整数k，如果数组中有两个不同的索引i和j可以使用nums[i] == nums[j] 和 abs(i - j) <= k，那么返回true;
 *
 * 示例：
 * 输入：nums = [1,2,3,1], k=3
 * 输出：true
 *
 * 输入：nums = [1,0,1,1], k=1
 * 输出：true
 *
 * 输入：nums = [1,2,3,1,2,3] k=2
 * 输出：false
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/contains-duplicate-ii/"> contains-duplicate-ii </a>
 */
public class ContainsDuplicateII {

    /**
     * 该方法超时
     */
    public boolean containsNearByDuplicate(int[] nums, int k) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] == nums[j]) {
                    if (Math.abs(i - j) <= k) {
                        return true;
                    }
                    break;
                }
            }
        }
        return false;
    }

    /**
     * 使用Set
     */
    public boolean containsNearByDuplicate0(int[] nums, int k) {
        final Set<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            // 移除元素，如果元素到nums[i]的距离大于k
            if (i > k) set.remove(nums[i - k - 1]);
            // 因为所有仍然存在的元素与nums[i]的距离都比K更近，
            // 因此如果add()返回false, 它意味着在距离K内存在同样值的元素，因此返回true
            if (!set.add(nums[i])) return true;
        }
        return false;
    }

    /**
     * 使用Map方法一
     */
    public boolean containsNearByDuplicate1(int[] nums, int k) {
        final HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            Integer lastIndex = map.put(nums[i], i);
            if (lastIndex != null && (i - lastIndex) <= k)
                return true;
        }
        return false;
    }

    /**
     * 使用Map方法二
     */
    public boolean containsNearByDuplicate2(int[] nums, int k) {
        final HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            // 判断是否包含该key
            if (map.containsKey(nums[i])) {
                // 如果当前值的索引值与前一个同一个值的最后一次出现的索引值的距离小于K, 返回true
                if (i - map.get(nums[i]) <= k) return true;
            }
            // 记录该值最后出现的索引值
            map.put(nums[i], i);
        }
        return false;
    }
}
