package algorithmEasy.rangeSumQuery;

/**
 * 题目描述：
 * 给定一个数组nums，处理以下类型的多个查询：
 * 1. 计算下标在left和right且left<=right之间的nums元素的和。
 *
 * 实现NumsArray类：
 * - NumsArray(int[] nums)使用整形数组nums初始化对象
 * - int sumRange(int left, int right) 返回下标在left和right且left<=right之间的nums元素的和。
 *
 * 示例：
 * 输入：["NumArray", "sumRange", "sumRange", "sumRange"]
 * [[[-2,0,3,-5,2,-1]], [0,2], [2,5], [0,5]]
 * 输出: [null, 1, -1, -3]
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/range-sum-query-immutable/"> range sum query immutable </a>
 */
public class RangeSumQuery {
    private final int[] nums;

    /**
     * 方法一： 从左索引开始，循环次数是右索引 + 1，便可得到结果。
     */
    public RangeSumQuery(int[] nums) {
        this.nums = nums;
    }

    public int sumRange(int left, int right) {
        int sum = 0;
        for (int i = left; i < right + 1; i++) {
            sum += nums[i];
        }
        return sum;
    }

    public static void main(String[] args) {
        final RangeSumQuery rangeSumQuery = new RangeSumQuery(new int[]{1, 1, 1, 1, 1, 1});
        System.out.println(rangeSumQuery.sumRange(1, 2));
    }

}

class RangeSumQuery0 {

    private final int[] nums;

    /**
     * 方法二: 在构造函数中对参数做处理
     */
    public RangeSumQuery0(int[] nums) {
        for (int i = 1; i < nums.length; i++) {
            nums[i] += nums[i - 1];
        }
          this.nums = nums;
    }

    public int sumRange(int left, int right) {
        if (left == 0)
            return nums[right];
        return nums[right] - nums[left - 1];
    }
}
