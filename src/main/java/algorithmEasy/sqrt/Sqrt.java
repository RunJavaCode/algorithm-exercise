package algorithmEasy.sqrt;

/**
 * 题目描述：
 * 实现一个能求出给定非负数的平方根的算法。
 * 如果开根号后的结果有小数，只返回整数部分。
 * 注意：不允许使用内置的指数函数或运算符，比如pow(x, 0.5)或者 x ** 0.5.
 *
 * 示例：
 * 输入: x = 4
 * 输出: 2
 *
 * 输入: x = 8
 * 输出: 2
 * 解释：因为8的平方根的值是2.82842..., 题目要求只截取整数部分，所以返回值是2
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/sqrtx/"> </a>
 */
public class Sqrt {

    public int mySqrt(int x) {
        // 特殊值的判断
        if (x == 0 || x == 1) return x;
        // 一个数开根号得到的最大值一定小于或等于该数的一半, 所以这里max设置可以成x/2。
        int left = 1, right = x / 2;
        // 二分法的思想
        while (left + 1 < right) {
            int mid = left + (right - left) / 2;
            // 为什么不使用 mid*mid > x? 因为那样写的话，当mid很大的时候会造成溢出
            if (mid > x / mid) {
                // 当中间值的平方大于x时，收缩右边的值
                right = mid - 1;
            } else {
                // 反之，收缩左边的值
                left = mid;
            }
        }
        // 比较得到最终结果值
        return x / right >= right ? right : left;
    }

}
