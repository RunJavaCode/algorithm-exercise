package algorithmEasy.removeLinkedListElements;

/**
 * 题目描述：
 * 给定一个Linked List的head，还有一个整数val，移除所有Node.val == val的节点，并且返回新的head。
 *
 * 示例：
 * 输入：head = [1,2,6,3,4,5,6], val = 6
 * 输出：[1,2,3,4,5]
 *
 * 输入：head = [], val = 1
 * 输出：[]
 *
 * 输入：head = [7,7,7,7], val = 7
 * 输出：[]
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/remove-linked-list-elements/"> Remove Linked List Elements </a>
 */
public class RemoveLinkedListElements {

    /**
     * 方法一：使用递归的方式
     * 关于使用递归方法的原理详细解释，我觉得最好的属于以下网友的解释，如下：
     * For someone who finds it difficult to understand:
     * When the input node is an empty node, then there is nothing to delete, so we just return a null node back. (That's the first line)
     * When the head of the input node is the target we want to delete, we just return head.next instead of head to skip it. (That's the third line), else we will return head.
     * We apply the same thing to every other node until it reaches null. (That's the second line).
     */
    public ListNode removeElements(ListNode head, int val) {
        if (head == null) return null;
        head.next = removeElements(head.next, val);
        return head.val == val ? head.next : head;
    }

    /**
     * 方法二：使用迭代的方式
     *
     */
    public ListNode removeElements1(ListNode head, int val) {
        // 作为一个假的开头使用
        ListNode fakeHead = new ListNode(0);
        fakeHead.next = head;
        // 定义两个变量，curr表示当前所指节点，prev表示前一个所指节点
        ListNode curr = head, prev = fakeHead;
        while (curr != null) {
            if (curr.val == val)
                // 如果节点值等于val, 直接就跳过curr当前节点，链接到下一个导入到节点。
                prev.next = curr.next;
            else
                prev = prev.next;
            curr = curr.next;
        }
        return fakeHead.next;
    }
}

// Definition for singly-linked list.
class ListNode {
    int val;
    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}
