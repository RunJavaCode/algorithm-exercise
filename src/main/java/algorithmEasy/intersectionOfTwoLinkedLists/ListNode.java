package algorithmEasy.intersectionOfTwoLinkedLists;

/**
 * 单向链表 list 定义
 */
public class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
        next = null;
    }
}
