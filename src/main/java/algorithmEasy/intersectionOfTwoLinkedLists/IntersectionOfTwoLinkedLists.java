package algorithmEasy.intersectionOfTwoLinkedLists;

/**
 * 题目描述：
 * 给定两个单向链表headA和headB，返回两个链表相交的节点。如果没有交点，那么null;
 *
 * 示例：
 * 需要图片示例，请参看原题链接里面具体的示例描述
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/intersection-of-two-linked-lists/">Intersection of Two Linked Lists</a>
 */
public class IntersectionOfTwoLinkedLists {

    /**
     * https://leetcode.com/problems/intersection-of-two-linked-lists/discuss/49785/Java-solution-without-knowing-the-difference-in-len!
     * 网友MyFavCat的解题, 非常巧妙。甚至有评论区网友说：I would never get a job if every programmer is as smart as you...
     * 有网友评论这个解答就像"romantic movie", "hey nodeB I will chase you till the day I meet you"。
     * 还有评论区点赞排名第一的网友Bryan Bo Cao对以上解答做的图解，一步步解释的非常清晰，真是太厉害了。
     */
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        // 边界条件检查
        if (headA == null || headB == null) return null;

        ListNode a = headA;
        ListNode b = headB;

        // 如果a和b有不同的长度，那在第二个迭代之后停止循环
        while (a != b) {
            // 在第一个迭代最后，只需要重置指针到另一个linkedList的头节点。
            a = a == null ? headB : a.next;
            b = b == null ? headA : b.next;
        }

        return a;
    }

    /**
     * 解法二
     */
    public ListNode getIntersectionNode1(ListNode headA, ListNode headB) {
        // 找到两个链表之间的差值大小
        int size = Math.abs(calculateSize(headA) - calculateSize(headB));

        // 如果headA的大小大于headB, 移动指针直到长度相等
        if (calculateSize(headA) > calculateSize(headB)) {
            for (int i = 0; i < size; i++) {
                headA = headA.next;
            }
        }
        // 如果headB的大小大于headA, 移动指针直到长度相等
        else {
            for (int i = 0; i < size; i++) {
                headB = headB.next;
            }
        }
        // 直到它们有相同的节点，向前移动
        while (headA != null && headB != null) {
            // 返回交点
            if (headA == headB) return headA;
            headA = headA.next;
            headB = headB.next;
        }
        return null;
    }

    // 用于计算单向列表的节点数
    private int calculateSize(ListNode node) {
        int size = 0;
        while (node != null) {
            size++;
            node = node.next;
        }
        return size;
    }
}
