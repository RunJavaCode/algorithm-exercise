package algorithmEasy.MinStack;

import java.util.Stack;

/**
 * 题目描述：
 * 设计一个stack,该stack支持在常量时间内push,pop,top,getMin获取最小元素。
 *
 * 实现MinStack类：
 * MinStack() 初始化MinStack对象
 * void push(val) 把val放到栈中
 * void pop() 移除栈中最顶部的元素
 * int top() 获取栈中最顶部的元素
 * int getMin() 获取栈中最小的元素
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/min-stack/">Min Stack</a>
 *
 * 该MinStack实现应该像如下方法实例化并调用
 * MinStack obj = new MinStack();
 * obj.push(val);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 *
 * 不使用Stack, 完全自行构造的方式。
 * 答案来源：https://leetcode.com/problems/min-stack/discuss/49010/Clean-6ms-Java-solution
 */
public class MinStack {
    private Node head;

    /** initialize your data structure here. */
    public MinStack() {
    }

    public void push(int val) {
        if (head == null) {
            head = new Node(val, val, null);
        } else {
            // 使用Math的min方法，比较当前值与传递进来的值，取最小值
            head = new Node(val, Math.min(val, head.min), head);
        }
    }

    public void pop() {
        head = head.next;
    }

    public int top() {
        return head.val;
    }

    public int getMin() {
        return head.min;
    }

    /**
     * 使用内部类构造。
     * 包含三个属性：当前值，最小值，下一个节点对象。
     */
    private static class Node {
        int val;
        int min;
        Node next;

        private Node(int val, int min, Node next) {
            this.val = val;
            this.min = min;
            this.next = next;
        }
    }
}

/**
 * 使用一个Stack
 * 来源：https://leetcode.com/problems/min-stack/discuss/49014/Java-accepted-solution-using-one-stack
 */
class MinStack0 {
    int min = Integer.MAX_VALUE;
    Stack<Integer> stack;

    public MinStack0() {
        stack = new Stack<>();
    }

    public void push(int x) {
        // 只有在push新值后，当前最小值发生变化时，才push旧的最小值x
        if (x <= min) {
            stack.push(min);
            // 这样保证min总是最小
            min = x;
        }
        stack.push(x);
    }

    public void pop() {
        // 如果pop操作会导致当前最小值的改变
        // pop两次并且将当前最小值更改为最后一个最小值
        if (stack.pop() == min)
            min = stack.pop();
    }

    public int top() {
        return stack.peek();
    }

    public int getMin() {
        return min;
    }
}

/**
 * 使用两个Stack
 * 来源：https://leetcode.com/problems/min-stack/discuss/49181/Java-solution-(accepted)
 */
class MinStack1 {
    // 保存其他值
    private final Stack<Integer> mStack = new Stack<>();
    // 保存最小值的Stack
    private final Stack<Integer> mMinStack = new Stack<>();

    public void push(int x) {
        mStack.push(x);
        if (mMinStack.size() != 0) {
            int min = mMinStack.peek();
            if (x <= min) {
                mMinStack.push(x);
            }
        } else {
            mMinStack.push(x);
        }
    }

    public void pop() {
        int x = mStack.pop();
        if (mMinStack.size() != 0) {
            if (x == mMinStack.peek()) {
                mMinStack.pop();
            }
        }
    }

    public int top() {
        return mStack.peek();
    }

    public int getMin() {
        return mMinStack.peek();
    }
}