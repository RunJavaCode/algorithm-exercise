package algorithmEasy.removeDuplicatesFromSortedList;

/**
 * 题目描述：
 * 给定一个名为head的已排序的链表, 删除所有重复的元素，也就是说，让每个元素只出现一次。
 * 返回的链表要保持排序。
 *
 * 示例一：
 * 输入：head = [1,1,2]
 * 输出：[1,2]
 *
 * 示例二：
 * 输入：head = [1,1,2,3,3]
 * 输出：[1,2,3]
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/remove-duplicates-from-sorted-list/">Remove Duplicates from Sorted List</a>
 */
public class RemoveDuplicatesFromSortedList {

    /**
     * 采用递归的方法
     */
    public ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null)
            return head;
        head.next = deleteDuplicates(head.next);
        return head.val == head.next.val ? head.next : head;
    }

    /**
     * 采用非递归的方法
     */
    public ListNode deleteDuplicates1(ListNode head) {
        ListNode current = head;
        while (current != null && current.next != null) {
            if (current.val == current.next.val)
                current.next = current.next.next;
            else
                current = current.next;
        }
        return head;
    }

}
