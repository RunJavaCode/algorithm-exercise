package algorithmEasy.containsDuplicate;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * 题目描述：
 * 给定一个整数数组nums，如果数组中有任何值出现了至少两次返回true，否则如果每个值都是不同的，返回false
 *
 * 示例：
 * 输入：nums = [1,2,3,1]
 * 输出：true
 *
 * 输入：nums = [1,2,3,4]
 * 输出：false
 *
 * 输入：nums = [1,1,1,3,3,4,3,2,4,2]
 * 输出：true
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/contains-duplicate/"> contains-duplicate </a>
 */
public class ContainsDuplicate {

    /**
     * 方法一：利用Set集合存储的元素不能重复的特性
     */
    public boolean containsDuplicate(int[] nums) {
        // set集合是不可重复的
        final Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            if (!set.add(num))
                return true;
        }
        return false;
    }

    /**
     * 方法二：双重循环
     *
     */
    public boolean containsDuplicate0(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] == nums[j])
                    return true;
            }
        }
        return false;
    }

    /**
     * 方法三：排序后逐对比较
     * 对数组做排序后，从索引1开始，与前一个值比较
     * index 1 --> index 0
     * index 2 --> index 1
     * ...
     * index n --> index n-1
     */
    public boolean containsDuplicate1(int[] nums) {
        // 利用Arrays类的sort方法排序
        Arrays.sort(nums);
        for (int i = 1; i < nums.length; i++) {
            // 后一个与前一个比较
            if(nums[i] == nums[i - 1])
                return true;
        }
        return false;
    }

}
