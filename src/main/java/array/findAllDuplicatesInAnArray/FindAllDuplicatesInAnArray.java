package array.findAllDuplicatesInAnArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 题目描述：
 * 给定一个长度为n的整型数组nums，其中所有的元素的范围都在[1,n]，并且每一个整数
 * 实现一次或两次，返回所有出现两次的整数组成的数组。
 *
 * 示例：
 * 输入：nums = [4,3,2,7,8,2,3,1]
 * 输出： [2,3]
 *
 * 输入：nums = [1,1,2]
 * 输出： [1]
 *
 * 输入：nums = [1]
 * 输出： []
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/find-all-duplicates-in-an-array/"> find-all-duplicates-in-an-array </a>
 */
public class FindAllDuplicatesInAnArray {

    /**
     * 方法一
     * 通过for循环把所有值作为key放入map中，用value记录对应key出现的次数。
     * 再通过增强for循环遍历map的entry判断value大于等于2的key值，添加到返回的结果数组中
     */
    public List<Integer> findDuplicates0(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            if (map.containsKey(num))
                map.put(num, map.get(num) + 1);
            else
                map.put(num, 1);
        }
        List<Integer> list = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() >= 2) {
                list.add(entry.getKey());
            }
        }
        return list;
    }

    /**
     * 方法二
     * 这个方法是网友提出的一种方法，很巧妙。不需要额外的空间占用。
     * 链接：https://leetcode.com/problems/find-all-duplicates-in-an-array/discuss/92387/Java-Simple-Solution
     */
    public List<Integer> findDuplicates1(int[] nums) {
        /*
          当找到一个数字 i 时，将位置 i-1 处的数字翻转为负数。
          如果位置 i-1 处的数字已经是负数，则 i 是出现两次的数字。
         */
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < nums.length; ++i) {
            int index = Math.abs(nums[i]) - 1;
            if (nums[index] < 0)
                list.add(index + 1);
            nums[index] = -nums[index];
        }
        return list;
    }
}
