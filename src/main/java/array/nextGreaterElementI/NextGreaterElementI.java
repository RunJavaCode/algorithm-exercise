package array.nextGreaterElementI;

import java.util.HashMap;
import java.util.Stack;

/**
 * 题目描述：
 * 一个数组中某个元素x的下一个更大的元素是同一个数组中x右边的第一个较大元素。
 * 您将获得两个不同的以0开头索引整数数组nums1和nums2，其中nums1是nums2的子集。
 *
 * 对于每个0 <= i < nums1.length，找到索引j, 使nums1[i] == nums2[j]，
 * 并确定nums2中nums2[j]的下一个较大元素。
 * 如果没有下一个更大的元素，那么这个查询的答案是-1。
 *
 * 示例：
 * 输入：nums1 = [4,1,2], nums2 = [1,3,4,2]
 * 输出：[-1,3,-1]
 *
 * 输入：nums1 = [2,4], nums2 = [1,2,3,4]
 * 输出：[3,-1]
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/next-greater-element-i/"> next greater element I </a>
 */
public class NextGreaterElementI {

    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        // 存储x与x的下一个较大元素的map
        HashMap<Integer, Integer> map = new HashMap<>();
        Stack<Integer> stack = new Stack<>();
        for (int num : nums2) {
            // 注意 while循环 与 if之间的区别
            while (!stack.isEmpty() && stack.peek() < num)
                map.put(stack.pop(), num);
            stack.push(num);
        }
        for (int i = 0; i < nums1.length; i++)
            // 新学到一个map的一个方法：getOrDefault
            nums1[i] = map.getOrDefault(nums1[i], -1);
        return nums1;
    }

}
