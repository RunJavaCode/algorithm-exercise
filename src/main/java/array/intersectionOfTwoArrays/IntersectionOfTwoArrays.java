package array.intersectionOfTwoArrays;

import java.util.HashSet;
import java.util.Set;

/**
 * 题目描述：
 * 给定两个整型数组nums1和nums2, 返回它们相交的数组。
 * 在结果集中的每个元素必须是唯一的，并且你可以以任何顺序返回结果集。
 *
 * 示例：
 * 输入：nums1 = [1,2,2,1], nums2 = [2,2]
 * 输出：[2]
 *
 * 输入：nums1 = [4,9,5], nums2 = [9,4,9,8,4]
 * 输出：[9,4] or [4,9]
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/intersection-of-two-arrays/"> Intersection of Two Arrays </a>
 */
public class IntersectionOfTwoArrays {

    /**
     * 方法：利用Set集合的元素不能重复的特性
     */
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> set = new HashSet<>();
        Set<Integer> interSet = new HashSet<>();
        for (int i : nums1) {
            set.add(i);
        }
        for (int i : nums2) {
            if (set.contains(i))
                interSet.add(i);
        }
        int[] result = new int[interSet.size()];
        int i = 0;
        for (Integer num : interSet) {
            result[i++] = num;
        }
        return result;
    }

}
