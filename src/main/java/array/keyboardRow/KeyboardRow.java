package array.keyboardRow;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.stream.Stream;

/**
 * 题目描述：
 * 给定一个words字符串数组，返回其中只能被美式键盘其中一行键入所有字母的单词。
 *
 * 在美式键盘中：
 * 第一行是：qwertyuiop
 * 第二行是：asdfghjkl
 * 第三行是：zxcvbnm
 *
 * 示例：
 * 输入: words = ["Hello","Alaska","Dad","Peace"]
 * 输出: ["Alaska","Dad"]
 *
 * 输入: words = ["omk"]
 * 输出: []
 *
 * 输入: words = ["adsdf","sfd"]
 * 输出: ["adsdf","sfd"]
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/keyboard-row/"> Keyboard Row </a>
 */
public class KeyboardRow {

    public String[] findWords(String[] words) {
        final List<String> answer = new ArrayList<>();
        String firstRow = "qwertyuiop";
        String secondRow = "asdfghjkl";
        String thirdRow = "zxcvbnm";
        for (String word : words) {
            // toLowerCase把所有字符转成小写
            final boolean b = matchingWord(word.toLowerCase(), word.toLowerCase().charAt(0));
            if (b) {
                answer.add(word);
            }
        }
        final String[] s = new String[answer.size()];
        for (int i = 0; i < answer.size(); i++) {
            s[i] = answer.get(i);
        }
        return s;
    }

    private boolean matchingWord(String s, char f) {
        String firstRow = "qwertyuiop";
        final char[] fr = firstRow.toCharArray();
        final Stack<Character> frStack = new Stack<>();
        for (char c : fr) {
            frStack.push(c);
        }
        String secondRow = "asdfghjkl";
        final char[] sr = secondRow.toCharArray();
        final Stack<Character> srStack = new Stack<>();
        for (char c : sr) {
            srStack.push(c);
        }
        String thirdRow = "zxcvbnm";
        final char[] tr = thirdRow.toCharArray();
        final Stack<Character> trStack = new Stack<>();
        for (char c : tr) {
            trStack.push(c);
        }
        final char[] sChar = s.toCharArray();
        if (frStack.contains(f)) {
            for (char c : sChar) {
                if (!frStack.contains(c))
                    return false;
            }
        } else if (srStack.contains(f)) {
            for (char c : sChar) {
                if (!srStack.contains(c))
                    return false;
            }
        } else if (trStack.contains(f)) {
            for (char c : sChar) {
                if (!trStack.contains(c))
                    return false;
            }
        }
        return true;
    }

    // 网友利用正则表达式和Java8新特性（Stream API\Lambda表达式\方法引用）来做
    // 很巧妙
    public String[] findWords1(String[] words) {
        return Stream.of(words)
                .filter(w -> w.toLowerCase().matches("[qwertyuiop]*|[asdfghjkl]*|[zxcvbnm]*"))
                .toArray(String[]::new);
    }

}
