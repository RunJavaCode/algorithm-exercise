package array.findAllNumbersDisappearedInAnArray;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * 题目描述：
 * 给定有n个整型元素的数组nums，nums[i]的范围在[1,n]内，返回在nums中没有出现在范围[1,n]中的所有整数组成的数组。
 *
 * 示例：
 * 输入：nums = [4,3,2,7,8,2,3,1]
 * 输出：[5,6]
 *
 * 输入：nums = [1,1]
 * 输出：[2]
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/"> find-all-numbers-disappeared-in-an-array </a>
 */
public class FindAllNumbersDisappearedInAnArray {

    /**
     * 方法一：利用Set集合的contain方法以及for循环nums数组进行判断
     */
    public List<Integer> findDisappearedNumbers0(int[] nums) {
        List<Integer> result = new ArrayList<>();
        final HashSet<Integer> set = new HashSet<>();
        for (int num : nums) {
            set.add(num);
        }
        for (int i = 1; i <= nums.length; i++) {
            if (!set.contains(i)) {
                result.add(i);
            }
        }
        return result;
    }

    /**
     * 方法二
     */
    public List<Integer> findDisappearedNumbers1(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            // 交换位置
            while (nums[i] != i + 1 && nums[i] != nums[nums[i] - 1]) {
                int tmp = nums[i];
                nums[i] = nums[tmp - 1];
                nums[tmp - 1] = tmp;
            }
        }
        List<Integer> rt = new ArrayList<>();
        for (int i = 1; i <= nums.length; i++) {
            if (nums[i] != i) {
                rt.add(i);
            }
        }
        return rt;
    }

}
