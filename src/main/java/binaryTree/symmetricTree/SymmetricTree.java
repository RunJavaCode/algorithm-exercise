package binaryTree.symmetricTree;

import binaryTree.TreeNode;
import javafx.util.Pair;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * 题目描述：
 * 给定一个二叉树root， 检查它是否是对称的。
 *
 * 示例一：
 * 输入：root = [1,2,2,3,4,3]
 * 输出：ture
 *
 * 示例二：
 * 输入：root = [1,2,2,null,3,null,3]
 * 输出：false
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/symmetric-tree/">Symmetric Tree</a>
 */
public class SymmetricTree {

    /**
     * 方式一：使用递归
     */
    public boolean isSymmetric(TreeNode root) {
        if (root == null || (root.left == null && root.right == null)) return true;
        return isMirror(root.left, root.right);
    }

    private boolean isMirror(TreeNode node1, TreeNode node2) {
        // 如果其中一边为null而另一边不为null，就是不对称的
        if ((node1 == null && node2 != null) || (node1 != null && node2 == null)) return false;
        // 根据前一个判断条件，这里只需要判断一个node即可, 这里判断node1
        if (node1 == null && node2 == null) return true;
        boolean flag = node1.val == node2.val;

        // 分别遍历左节点树和右节点树是否对称
        boolean left = isMirror(node1.left, node2.right);
        boolean right = isMirror(node1.right, node2.left);

        return flag && left && right;
    }

    /**
     * 方式二：使用Queue
     */
    public boolean isSymmetric1(TreeNode root) {
        Queue<Pair<TreeNode, TreeNode>> queue = new LinkedList<>();
        queue.add(new Pair<>(root, root));
        while (!queue.isEmpty()) {
            final Pair<TreeNode, TreeNode> nodes = queue.poll();
            final TreeNode n1 = nodes.getKey();
            final TreeNode n2 = nodes.getValue();
            if (n1 != null || n2 != null) {
                if (n1 != null && n2 != null && n1.val == n2.val) {
                    queue.add(new Pair<>(n1.left, n2.right));
                    queue.add(new Pair<>(n1.right, n2.left));
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 方式三：使用Stack
     */
    public boolean isSymmetric2(TreeNode root) {
        Stack<Pair<TreeNode, TreeNode>> stack = new Stack<>();
        stack.push(new Pair<>(root, root));
        while (!stack.isEmpty()) {
            final Pair<TreeNode, TreeNode> pop = stack.pop();
            final TreeNode key = pop.getKey();
            final TreeNode value = pop.getValue();
            if (key != null || value != null) {
                if (key != null && value != null && key.val == value.val) {
                    stack.push(new Pair<>(key.left, value.right));
                    stack.push(new Pair<>(key.right, value.left));
                } else {
                    return false;
                }
            }
        }
        return true;
    }
}
