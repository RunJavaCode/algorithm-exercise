package binaryTree.binaryTreePreorderTraversal;

import binaryTree.TreeNode;

import java.util.*;

/**
 * 题目描述：
 * 给定一个二叉树root,返回该二叉树的节点值前序遍历结果
 *
 * 示例：
 * 输入：root = [1,null,2,3]
 * 输出：[1,2,3]
 *
 * 输入：root = []
 * 输出：[]
 *
 * 输入：root = [1]
 * 输出：[1]
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/binary-tree-preorder-traversal/">Binary Tree Preorder Traversal</a>
 */
public class BinaryTreePreorderTraversal {

    /**
     * 前序遍历的节点遍历顺序：root -> left -> right
     * 遍历写法如下：
     */
    private void preorder(TreeNode root) {
        if (root == null) return;
        System.out.println(root.val);
        preorder(root.left);
        preorder(root.right);
    }

    /**
     * 递归方法一
     */
    private final ArrayList<Integer> result = new ArrayList<>();
    public List<Integer> preorderTraversal0(TreeNode root) {
        if (root == null)
            return result;
        else
            result.add(root.val);
        preorderTraversal0(root.left);
        preorderTraversal0(root.right);
        return result;
    }

    /**
     * 递归方法二
     */
    public List<Integer> preorderTraversal1(TreeNode root) {
        return recursePreorder(root, new ArrayList<>());
    }
    private List<Integer> recursePreorder(TreeNode curNode, List<Integer> list) {
        if (curNode == null)
            return list;
        list.add(curNode.val);
        recursePreorder(curNode.left, list);
        recursePreorder(curNode.right, list);
        return list;
    }

    /**
     * 迭代方式，使用Stack
     * 注意stack的特点：先进后出
     */
    public List<Integer> preorderTraversal2(TreeNode root) {
        ArrayList<Integer> answer = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode curNode = stack.pop();
            if (curNode != null) {
                answer.add(curNode.val);
                // 这里要先push右节点。
                // 原来这是根据stack的特点决定的：先进后出
                stack.push(curNode.right);
                stack.push(curNode.left);
            }
        }
        return answer;
    }

    /**
     * 使用Stack第二种写法
     */
    public List<Integer> preorderTraversal3(TreeNode root) {
        ArrayList<Integer> answer = new ArrayList<>();
        if (root == null) return answer;
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode curNode = stack.pop();
            answer.add(curNode.val);
            // 这里要先push右节点。
            // 原来这是根据stack的特点决定的：先进后出
            if (curNode.right != null)
                stack.push(curNode.right);
            if (curNode.left != null)
                stack.push(curNode.left);
        }
        return answer;
    }

    /**
     * 使用LinkedList
     */
    public List<Integer> preorderTraversal4(TreeNode root) {
        LinkedList<Integer> result = new LinkedList<>();
        Deque<TreeNode> deque = new ArrayDeque<>();
        TreeNode cur = root;
        while (!deque.isEmpty() || cur != null) {
            if (cur != null) {
                deque.push(cur);
                // 添加root根节点值
                result.add(cur.val);
                cur = cur.left;
            } else {
                TreeNode node = deque.pop();
                cur = node.right;
            }
        }
        return result;
    }
}
