package binaryTree.maximumDepthOfBinaryTree;

import binaryTree.TreeNode;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * 题目描述：
 * 给定一个二叉树root, 返回它的最大深度
 * 二叉树的最大深度是指 从根节点沿着最长的路径到最远的叶子节点的节点数。
 *
 * 示例一：
 * 输入：root = [3,9,20,null,null,15,7]
 * 输出：3
 *
 * 示例二：
 * 输入：root = [1,null,2]
 * 输出：2
 *
 * 示例二：
 * 输入：root = []
 * 输出：0
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/maximum-depth-of-binary-tree/"> maximum depth of binary tree</a>
 */
public class MaximumDepthOfBinaryTree {

    /**
     * 方式一：递归
     * 这个方式最简洁。
     */
    public int maxDepth0(TreeNode root) {
        if (root == null) return 0;
        return 1 + Math.max(maxDepth0(root.left), maxDepth0(root.right));
    }

    /**
     * 方法二：迭代, 使用 BFS(Breath First Search)
     * 该方法比 使用 DFS 快
     */
    public int maxDepth1(TreeNode root) {
        if (root == null) return 0;
        Queue<TreeNode> nodes = new LinkedList<>();
        nodes.add(root);
        int depth = 0;
        while (!nodes.isEmpty()) {
            int size = nodes.size();
            while (size-- > 0) {
                TreeNode cur = nodes.poll();
                if (cur.left != null) {
                    nodes.add(cur.left);
                }
                if (cur.right != null) {
                    nodes.add(cur.right);
                }
            }
            // 每向下一层，depth加1;
            depth++;
        }
        return depth;
    }

    /**
     * 方式三：迭代，使用 DFS(Depth First Search)
     * 该方法比 BFS 慢
     */
    public int maxDepth2(TreeNode root) {
        if (root == null) return 0;
        Stack<TreeNode> stack = new Stack<>();
        Stack<Integer> value = new Stack<>();
        stack.push(root);
        value.push(1);
        int max = 0;
        while (!stack.isEmpty()) {
            TreeNode node = stack.pop();
            int temp = value.pop();
            max = Math.max(temp, max);
            if (node.left != null) {
                stack.push(node.left);
                value.push(temp + 1);
            }
            if (node.right != null) {
                stack.push(node.right);
                value.push(temp + 1);
            }
        }
        return max;
    }
}
