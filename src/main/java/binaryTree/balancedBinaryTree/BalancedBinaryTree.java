package binaryTree.balancedBinaryTree;

import binaryTree.TreeNode;

/**
 * 题目描述：
 * 给一个二叉树，判断它是不是"高度平衡"(height-balanced)的。
 * 对于这个问题，"高度平衡"的定义是：
 * 该二叉树下每个节点的左右子树高度相差不超过1；
 *
 * 示例：
 * 输入：root = [3,9,20,null,null,15,7]
 * 输出：true
 *
 * 输出：root = [1,2,2,3,3,null,null,4,4]
 * 输出：false
 *
 * 输出：root = []
 * 输出：true
 *
 * 原题链接：
 *
 * @see <a href="https://leetcode.com/problems/balanced-binary-tree/">balanced binary tree</a>
 */
public class BalancedBinaryTree {

    /**
     * 递归的方式一
     */
    public boolean isBalanced(TreeNode root) {
        if (root == null) return true;
        if(Math.abs(height(root.right) - height(root.right)) > 1)
            return false;
        else
            return isBalanced(root.left) && isBalanced(root.right);
    }

    private int height(TreeNode root){
        return root == null ? 0 : 1 + Math.max(height(root.left), height(root.right));
    }

    /**
     * 类似上面一种写法但以更容易理解的方式
     */
    public boolean isBalanced0(TreeNode root) {
        if (root == null) return true;
        int leftHeight = height0(root.left);
        int rightHeight = height0(root.right);
        if (Math.abs(leftHeight - rightHeight) > 1)
            return false;
        return isBalanced(root.left) && isBalanced(root.right);
    }

    private int height0(TreeNode root) {
        if (root == null) return 0;
        int leftHeight = height0(root.left);
        int rightHeight = height0(root.right);
        if (leftHeight > rightHeight)
            return leftHeight + 1;
        return rightHeight + 1;
    }

    /**
     * todo 以迭代的方式
     */
    public boolean isBalanced1(TreeNode root) {

        return true;
    }

}
