package binaryTree.binaryTreeInOrderTraversal;

import binaryTree.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * 题目描述：
 * 给定一个二叉树的root，返回其节点值的中序遍历。
 *
 * 示例一：
 * 输入： root = [1,null,2,3]
 * 输出： [1,3,2]
 *
 * 示例二：
 * 输入： root = []
 * 输出： []
 *
 * 示例二：
 * 输入： root = [1]
 * 输出： [1]
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/binary-tree-inorder-traversal/">Binary Tree Inorder Traversal</a>
 */
public class BinaryTreeInOrderTraversal {

    /**
     * 实现一: 采用递归的方式。
     */
    public List<Integer> inorderTraversal0(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        traverse(root, result);
        return result;
    }

    private void traverse(TreeNode root, List<Integer> list) {
        if (root != null) {
            traverse(root.left, list);
            list.add(root.val);
            traverse(root.right, list);
        }
    }

    /**
     * 实现二: 借助Stack， 采用迭代的方式。
     */
    public List<Integer> inorderTraversal1(TreeNode root) {
        ArrayList<Integer> result = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        while (root != null || !stack.empty()){
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            TreeNode poppedNode = stack.pop();
            result.add(poppedNode.val);
            root = poppedNode.right;
        }
        return result;
    }

}
