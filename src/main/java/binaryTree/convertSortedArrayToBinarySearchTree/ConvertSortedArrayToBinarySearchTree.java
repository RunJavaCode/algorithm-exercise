package binaryTree.convertSortedArrayToBinarySearchTree;

import binaryTree.TreeNode;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

/**
 * 题目描述：
 * 给定一个整数数组nums,其中元素以升序排列，把这个数组转成“高度平衡”二叉搜索树。
 * 所谓“高度平衡”二叉搜索树是一棵每个节点的两个子树的深度不会超过1的二叉树。
 *
 * 示例一：
 * 输入: nums = [-10,-3,0,5,9]
 * 输出: [0,-3,9,-10,null,5]
 * 解释: [0,-10,5,null,-3,null,9] 也是可接受的答案
 *
 * 示例二：
 * 输入: nums = [1,3]
 * 输出: [3,1]
 * 解释: [1,3] and [3,1] 都是”高度平衡“的BST(Binary Search Tree的缩写).
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/">Convert sorted array to binary search tree</a>
 */
public class ConvertSortedArrayToBinarySearchTree {

    /**
     * 采用递归的方式
     */
    public TreeNode sortedArrayToBST(int[] nums) {
        int mid = nums.length / 2;
        TreeNode root = new TreeNode(nums[mid]);
        return populateTree(nums, 0, nums.length - 1, root);
    }

    private TreeNode populateTree(int[] nums, int begin, int end, TreeNode root) {
        if (begin > end) {
            return null;
        }
        // 取中间值的方式
        int mid = begin + (end - begin) / 2;
        TreeNode node = new TreeNode(nums[mid]);
        node.left = populateTree(nums, begin, mid - 1, root);
        node.right = populateTree(nums, mid + 1, end, root);
        return node;
    }

    /**
     * 迭代方式 : 使用三个Deque
     */
    public TreeNode sortedArrayToBST0(int[] nums) {
        int len = nums.length;
        if (len == 0) return null;

        // 0作为占位符
        TreeNode head = new TreeNode(0);

        // 三个队列
        // 存储要进行处理的node节点
        Deque<TreeNode> nodeDeque = new LinkedList<>();
        nodeDeque.push(head);
        // 用来存储需要从nums读取的node的范围
        Deque<Integer> leftIndexDeque = new LinkedList<>();
        leftIndexDeque.push(0);
        Deque<Integer> rightIndexDeque = new LinkedList<>();
        rightIndexDeque.push(len - 1);

        while (!nodeDeque.isEmpty()) {
            TreeNode curNode = nodeDeque.pop();
            int left = leftIndexDeque.pop();
            int right = rightIndexDeque.pop();
            // 这样写可以避免溢出
            int mid = left + (right - left) / 2;
            if (left <= mid - 1) {
                 curNode.left = new TreeNode(0);
                 nodeDeque.push(curNode.left);
                 leftIndexDeque.push(left);
                 rightIndexDeque.push(mid - 1);
            }
            if (mid + 1 <= right) {
                curNode.right = new TreeNode(0);
                nodeDeque.push(curNode.right);
                leftIndexDeque.push(mid + 1);
                rightIndexDeque.push(right);
            }
        }
        return head;
    }

    /**
     * 迭代方式 : 使用一个Stack
     */
    public TreeNode sortedArrayToBST1(int[] nums) {
        TreeNode root = new TreeNode(0);

        Stack<Node> stack = new Stack<>();
        Node node = new Node(root, 0, nums.length - 1);
        stack.push(node);
        while (!stack.isEmpty()) {
            Node cur = stack.pop();
            int mid = cur.left + (cur.right - cur.left) / 2;
            cur.node.val = nums[mid];
            if (cur.left < mid) {
                cur.node.left = new TreeNode(0);
                stack.push(new Node(cur.node.left, cur.left, mid - 1));
            }
            if (cur.right > mid) {
                cur.node.right = new TreeNode(0);
                stack.push(new Node(cur.node.right, mid + 1, cur.right));
            }
        }
        return root;
    }

    /**
     * 内部类
     */
    private static class Node {
        TreeNode node;
        int left, right;
        public Node (TreeNode node, int left, int right) {
            this.node = node;
            this.left = left;
            this.right = right;
        }
    }

}
