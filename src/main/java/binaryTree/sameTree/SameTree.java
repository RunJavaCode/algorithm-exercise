package binaryTree.sameTree;

import binaryTree.TreeNode;

import java.util.*;

/**
 * 题目描述：
 * 给两个二叉树p和q，实现一个方法检查它们是不是相同的。
 * 两个二叉树相同是指结构上和每个节点上的值都相同。
 *
 * 示例一：
 * 输入： p = [1,2,3], q = [1,2,3]
 * 输出： true
 *
 * 示例二：
 * 输入： p = [1,2], q = [1,null,2]
 * 输出： false
 *
 * 示例三：
 * 输入： p = [1,2,1], q = [1,1,2]
 * 输出： false
 *
 * 原题链接：
 *
 * @see <a href="https://leetcode.com/problems/same-tree/">Same Tree</a>
 */
public class SameTree {

    /**
     * 递归的方式实现
     */
    public boolean isSameTree(TreeNode p, TreeNode q) {
        // 判断子树是否都为null
        if (p == null && q == null)
            return true;
        // 判断子树是否有一个为null
        if (p == null || q == null)
            return false;
        // 比较两个子树的值,不相等直接返回false
        if (p.val != q.val)
            return false;
        // 递归调用,直到遍历所有的子树
        return isSameTree(p.left, q.left)
                && isSameTree(p.right, q.right);
    }

    /**
     * 使用Stack对二叉树做前序遍历比较
     */
    public boolean isSameTree1(TreeNode p, TreeNode q) {
        Stack<TreeNode> pStack = new Stack<>();
        Stack<TreeNode> qStack = new Stack<>();
        // TreeNode不为null，才push进Stack里面, 否则size不为空，后面会造成空指针异常
        if (p != null) pStack.push(p);
        if (q != null) qStack.push(q);
        // 这个过程在不断比较两个TreeNode的左右节点的值以及大小
        while (!pStack.empty() && !qStack.empty()) {
            TreeNode pop_p = pStack.pop();
            TreeNode pop_q = qStack.pop();
            // 比较节点值
            if (pop_p.val != pop_q.val) return false;

            // 左节点不为null的情况下， 把左节点push到stack中
            if (pop_p.left != null) pStack.push(pop_p.left);
            if (pop_q.left != null) qStack.push(pop_q.left);

            // 再次比较两个stack的size大小
            if (pStack.size() != qStack.size()) return false;

            // 重复右边节点, 并比较
            if (pop_p.right != null) pStack.push(pop_p.right);
            if (pop_q.right != null) qStack.push(pop_q.right);

            if (pStack.size() != qStack.size()) return false;
        }
        // 返回两个Stack的size大小比较结果
        return pStack.size() == qStack.size();
    }

    /**
     * 使用单个 queue 实现
     */
    public boolean isSameTree2(TreeNode p, TreeNode q) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(p);
        queue.add(q);
        while (!queue.isEmpty()) {
            TreeNode f = queue.poll();
            TreeNode s = queue.poll();
            // 都为null继续执行
            if (f == null && s == null) {
                continue;
                // 如果其中一个为null, 或者值不想等，返回false结果
            } else if (f == null || s == null || f.val != s.val) {
                return false;
            }
            // 依次把左、右子节点加入队列
            queue.add(f.left);
            queue.add(s.left);
            queue.add(f.right);
            queue.add(s.right);
        }
        return true;
    }

    public static void main(String[] args) {
        // 有关Stack的存放null值测试
        Stack<TreeNode> stack = new Stack<>();
        // push是Stack本身新增的方法
        stack.push(null);
        // add是Stack继承Vector的方法
        stack.add(null);
        // pop出来的值是： null 。
        // 如果后面继续误使用该pop出来的值容易造成空指针。可以使用peek方法对值做是否为null判断后再使用。
        System.out.println(stack.pop());
        // size大小是：2
        System.out.println(stack.size());

        // 有关Queue存放null值的测试
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(null);
        queue.add(null);
        // poll出来的是null值
        System.out.println(queue.poll());
        // size大小： 2
        System.out.println(queue.size());
    }
}
