package binaryTree.minimumDepthOfBinaryTree;

import binaryTree.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 题目描述：
 * 给定一个二叉树，找到它的最小的深度。
 * 最小深度是指根节点到最近叶节点的最短路径上的节点数。
 *
 * 示例：
 * 输入：root = [3,8,20,null,null,15,7]
 * 输出：2
 *
 * 输入：root = [3,8,20,null,null,15,7]
 * 输出：5
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/minimum-depth-of-binary-tree/">Minimum Depth Of Binary Tree</a>
 */
public class MinimumDepthOfBinaryTree {

    /**
     * 递归的方式
     */
    public int minDepth(TreeNode root) {
        // 基准条件
        if (root == null)
            return 0;
        if (root.left == null && root.right == null)
            return 1;
        // 满足二叉树最小深度的要求
        if (root.left == null)
            return minDepth(root.right) + 1;
        if (root.right == null)
            return minDepth(root.left) + 1;
        return Math.min(minDepth(root.left), minDepth(root.right)) + 1;
    }

    /**
     * 迭代的方式，使用队列实现
     * 这里用到了BFS(Breath First Search:广度优先遍历的算法)
     */
    public int minDepth0(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        int depth = 0;
        if (root != null)
            queue.add(root);

        while (!queue.isEmpty()) {
            depth++;
            for(int count = queue.size(); count > 0; count--) {
                TreeNode cur = queue.poll();
                // 找到第一个叶子节点后直接返回深度
                if (cur.left == null && cur.right == null)
                    return depth;
                // 否则把子树加入队列，继续遍历
                if (cur.left != null)
                    queue.add(cur.left);
                if (cur.right != null)
                    queue.add(cur.right);
            }
        }
        return depth;
    }
}
