package binaryTree.binaryTreePostorderTraversal;

import binaryTree.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * 题目描述：
 * 给定一个二叉树root,返回该二叉树的节点值后序遍历结果
 *
 * 示例：
 * 输入：root = [1,null,2,3]
 * 输出：[3,2,1]
 *
 * 输入：root = []
 * 输出：[]
 *
 * 输入：root = [1]
 * 输出：[1]
 *
 *
 * 原题链接：
 * @see <a href="https://leetcode.com/problems/binary-tree-postorder-traversal/">Binary Tree Postorder Traversal</a>
 *
 */
public class BinaryTreePostorderTraversal {

    /**
     * 迭代的方式一
     */
    public static List<Integer> postorderTraversal(TreeNode root) {
        LinkedList<Integer> linkedList = new LinkedList<>();
        if (root == null) return linkedList;
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode node = stack.pop();
            // 利用链表的特性,每次加在第一个，产生倒序的效果
            linkedList.addFirst(node.val);
            // 因为是倒序加入LinkedList，所以这里要先加左节点
            if (node.left != null)
                stack.push(node.left);

            if (node.right != null)
                stack.push(node.right);
        }
        return linkedList;
    }

    /**
     * 迭代的方式二
     * 放入栈的顺序，先右边再左边
     */
    public static List<Integer> postorderTraversal1(TreeNode root) {
        final LinkedList<Integer> linkedList = new LinkedList<>();
        Stack<TreeNode> stack = new Stack<>();
        TreeNode cur = root;
        // 由两个条件组合： 当前节点不为null；stack栈不为空
        while (cur != null || !stack.isEmpty()) {
            // 当前不为null
            if (cur != null) {
                // 把节点放入stack栈
                stack.push(cur);
                // 往LinkedList头部添加节点值    注意：如果这里使用 add方式，那么就是前序遍历了
                linkedList.addFirst(cur.val);
                // 把当前节点的右边节点赋值为当前节点
                cur = cur.right;
            } else {
                // 从栈中拿出节点，把拿出节点的左节点赋值为当前节点
                TreeNode node = stack.pop();
                cur = node.left;
            }
        }
        return linkedList;
    }

    /**
     * 递归的方式
     */
    public static List<Integer> postorderTraversal2(TreeNode root) {
        return post(root, new ArrayList<>());
    }
    private static List<Integer> post(TreeNode root, List<Integer> list) {
        if (root == null)
            return list;
        post(root.left, list);
        post(root.right, list);
        list.add(root.val);
        return list;
    }

    public static void main(String[] args) {
        final TreeNode node = new TreeNode(1);
        node.right = new TreeNode(2);
        node.right.left = new TreeNode(3);
        final List<Integer> list = postorderTraversal1(node);
        for (Integer integer : list) {
            System.out.print(integer + " ");
        }
    }

}
